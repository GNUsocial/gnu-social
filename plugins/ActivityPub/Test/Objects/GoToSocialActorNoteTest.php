<?php

declare(strict_types = 1);

// {{{ License

// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.

// }}}

namespace Plugin\ActivityPub\Test\Objects;

use App\Core\DB;
use App\Core\VisibilityScope;
use App\Entity\Actor;
use App\Util\GNUsocialTestCase;
use Plugin\ActivityPub\Entity\ActivitypubActor;
use Plugin\ActivityPub\Entity\ActivitypubRsa;
use Plugin\ActivityPub\Util\Model\Actor as APActor;
use Plugin\ActivityPub\Util\Model\Note as APNote;

class GoToSocialActorNoteTest extends GNUsocialTestCase
{
    public function testGtSActorAndNoteFromJson1()
    {
        self::bootKernel();

        // actor first
        $ap_actor_path = GoToSocialTest::fixturesPath('objects/person_1.jsonld');
        $ap_actor = APActor::fromJson(fread(fopen($ap_actor_path, 'r'), filesize($ap_actor_path)));
        DB::flush();
        
        static::assertNotNull($ap_actor);
        static::assertSame('https://example.org/users/admin/inbox', $ap_actor->getInboxUri());
        static::assertNull($ap_actor->getInboxSharedUri());
        $ap_actor_uri = $ap_actor->getUri();
        static::assertSame('https://example.org/users/admin', $ap_actor_uri);

        $ap_profile = DB::findOneBy(ActivitypubActor::class, ['uri' => $ap_actor_uri], return_null: true);
        static::assertNotNull($ap_profile);
        static::assertInstanceOf(ActivitypubActor::class, $ap_profile);
        static::assertSame('https://example.org/users/admin/inbox', $ap_profile->getInboxUri());
        static::assertNull($ap_profile->getInboxSharedUri());

        $actor = $ap_profile->getActor();
        static::assertSame('https://example.org/users/admin', $actor->getUri());
        static::assertSame(Actor::PERSON, $actor->getType());
        static::assertSame('admin', $actor->getNickname());
        static::assertNull($actor->getFullname());
        $public_key = ActivitypubRsa::getByActor($actor)->getPublicKey();
        static::assertSame("-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqtQQjwFLHPez+7uF9AX7\nuvLFHm3SyNIozhhVmGhxHIs0xdgRnZKmzmZkFdrFuXddBTAglU4C2u3dw10jJO1a\nWIFQF8bGkRHZG7Pd25/XmWWBRPmOJxNLeWBqpj0G+2zTMgnAV72hALSDFY2/QDsx\nUthenKw0Srpj1LUwvRbyVQQ8fGu4v0HACFnlOX2hCQwhfAnGrb0V70Y2IJu++MP7\n6i49md0vR0Mv3WbsEJUNp1fTIUzkgWB31icvfrNmaaAxw5FkAE+KfkkylhRxi5i5\nRR1XQUINWc2Kj2Kro+CJarKG+9zasMyN7+D230gpESi8rXv1SwRu865FR3gANdDS\nMwIDAQAB\n-----END PUBLIC KEY-----\n", $public_key);

        $actor_id = $ap_profile->getActorId();
        static::assertNotNull($actor_id);
        static::assertIsNumeric($actor_id);
        static::assertNotEquals(0, $actor_id);

        // now a note belonging to the actor
        $ap_note_path = GoToSocialTest::fixturesPath('objects/note_1.jsonld');
        $ap_note = APNote::fromJson(fread(fopen($ap_note_path, 'r'), filesize($ap_note_path)));
        static::assertNotNull($ap_note);
        static::assertSame('text/html', $ap_note->getContentType(), $ap_note->getContentType());
        static::assertSame('hello world! #welcome ! first post on the instance :rainbow: !', $ap_note->getContent(), $ap_note->getContent());
        static::assertSame('hello world! #welcome ! first post on the instance :rainbow: !', $ap_note->getRendered(), $ap_note->getRendered());
        static::assertSame('ActivityPub', $ap_note->getSource());
        static::assertNull($ap_note->getReplyTo());
        static::assertFalse($ap_note->getIsLocal());
        static::assertSame(VisibilityScope::EVERYWHERE, $ap_note->getScope());
        static::assertSame('https://example.org/@admin/statuses/01F8MH75CBF9JFX4ZAD54N0W0R', $ap_note->getUrl());
        static::assertNull($ap_note->getLanguageLocale());
        static::assertSame('note', $ap_note->getType());
        static::assertNull($ap_note->getTitle());
    }

    public function testGtSActorAndNoteFromJson2()
    {
        self::bootKernel();

        // actor first
        $ap_actor_path = GoToSocialTest::fixturesPath('objects/person_2.jsonld');
        $ap_actor = APActor::fromJson(fread(fopen($ap_actor_path, 'r'), filesize($ap_actor_path)));
        DB::flush();
        
        static::assertNotNull($ap_actor);
        static::assertSame('https://example.org/users/the_mighty_zork/inbox', $ap_actor->getInboxUri());
        static::assertNull($ap_actor->getInboxSharedUri());
        $ap_actor_uri = $ap_actor->getUri();
        static::assertSame('https://example.org/users/the_mighty_zork', $ap_actor_uri);

        $ap_profile = DB::findOneBy(ActivitypubActor::class, ['uri' => $ap_actor_uri], return_null: true);
        static::assertNotNull($ap_profile);
        static::assertInstanceOf(ActivitypubActor::class, $ap_profile);
        static::assertSame('https://example.org/users/the_mighty_zork/inbox', $ap_profile->getInboxUri());
        static::assertNull($ap_profile->getInboxSharedUri());

        $actor = $ap_profile->getActor();
        static::assertSame('https://example.org/users/the_mighty_zork', $actor->getUri());
        static::assertSame(Actor::PERSON, $actor->getType());
        static::assertSame('the_mighty_zork', $actor->getNickname());
        static::assertSame('original zork (he/they)', $actor->getFullname());
        $public_key = ActivitypubRsa::getByActor($actor)->getPublicKey();
        static::assertSame("-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwXTcOAvM1Jiw5Ffpk0qn\nr0cwbNvFe/5zQ+Tp7tumK/ZnT37o7X0FUEXrxNi+dkhmeJ0gsaiN+JQGNUewvpSk\nPIAXKvi908aSfCGjs7bGlJCJCuDuL5d6m7hZnP9rt9fJc70GElPpG0jc9fXwlz7T\nlsPb2ecatmG05Y4jPwdC+oN4MNCv9yQzEvCVMzl76EJaM602kIHC1CISn0rDFmYd\n9rSN7XPlNJw1F6PbpJ/BWQ+pXHKw3OEwNTETAUNYiVGnZU+B7a7bZC9f6/aPbJuV\nt8Qmg+UnDvW1Y8gmfHnxaWG2f5TDBvCHmcYtucIZPLQD4trAozC4ryqlmCWQNKbt\n0wIDAQAB\n-----END PUBLIC KEY-----\n", $public_key);

        $actor_id = $ap_profile->getActorId();
        static::assertNotNull($actor_id);
        static::assertIsNumeric($actor_id);
        static::assertNotEquals(0, $actor_id);

        // now a note belonging to the actor
        $ap_note_path = GoToSocialTest::fixturesPath('objects/note_2.jsonld');
        $ap_note = APNote::fromJson(fread(fopen($ap_note_path, 'r'), filesize($ap_note_path)));
        static::assertNotNull($ap_note);
        static::assertSame('text/html', $ap_note->getContentType(), $ap_note->getContentType());
        static::assertSame('hello everyone!', $ap_note->getContent(), $ap_note->getContent());
        static::assertSame('hello everyone!', $ap_note->getRendered(), $ap_note->getRendered());
        static::assertSame('ActivityPub', $ap_note->getSource());
        static::assertNull($ap_note->getReplyTo());
        static::assertFalse($ap_note->getIsLocal());
        static::assertSame(VisibilityScope::EVERYWHERE, $ap_note->getScope());
        static::assertSame('https://example.org/@the_mighty_zork/statuses/01F8MHAMCHF6Y650WCRSCP4WMY', $ap_note->getUrl());
        static::assertNull($ap_note->getLanguageLocale());
        static::assertSame('note', $ap_note->getType());
        static::assertSame('introduction post', $ap_note->getTitle());
    }
}
