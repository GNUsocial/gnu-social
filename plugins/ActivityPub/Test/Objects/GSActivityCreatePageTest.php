<?php

declare(strict_types = 1);

// {{{ License

// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.

// }}}

namespace Plugin\ActivityPub\Test\Objects;

use App\Util\GNUsocialTestCase;
use Plugin\ActivityPub\Entity\ActivitypubActivity;
use Plugin\ActivityPub\Util\Explorer;

class GSActivityCreatePageTest extends GNUsocialTestCase
{
    public function testNoteFromJson()
    {
        self::bootKernel();

        $activity_uri = 'https://instance.gnusocial.test/activity/1338';
        $group_uri    = 'https://instance.gnusocial.test/actor/21';
        $ap_activity  = ActivitypubActivity::getByPK(['activity_uri' => $activity_uri]);

        $activity = $ap_activity->getActivity();
        static::assertSame('create', $activity->getVerb());
        static::assertSame('note', $activity->getObjectType());
        static::assertSame('ActivityPub', $activity->getSource());

        static::assertCount(1, $attT = $ap_activity->getAttentionTargets());
        static::assertObjectEquals(Explorer::getOneFromUri($group_uri, try_online: false), $attT[0]);
    }
}
