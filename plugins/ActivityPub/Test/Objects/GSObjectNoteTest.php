<?php

declare(strict_types = 1);

// {{{ License

// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.

// }}}

namespace Plugin\ActivityPub\Test\Objects;

use App\Core\VisibilityScope;
use App\Entity\Note;
use App\Util\GNUsocialTestCase;
use Plugin\ActivityPub\ActivityPub;
use Plugin\ActivityPub\Entity\ActivitypubObject;
use Plugin\ActivityPub\Util\Explorer;

class GSObjectNoteTest extends GNUsocialTestCase
{
    public function testNoteFromJson()
    {
        self::bootKernel();

        $actor_uri  = 'https://instance.gnusocial.test/actor/42';
        $object_uri = 'https://instance.gnusocial.test/object/note/1337';
        $note       = ActivityPub::getObjectByUri($object_uri, try_online: false);
        static::assertInstanceOf(Note::class, $note);

        static::assertSame(Explorer::getOneFromUri($actor_uri)->getId(), $note->getActorId());
        static::assertSame('text/plain', $note->getContentType());
        static::assertSame('hello, world.', $note->getContent());
        static::assertSame('<p>hello, world.</p>', $note->getRendered());
        static::assertSame('ActivityPub', $note->getSource());
        static::assertNull($note->getReplyTo());
        static::assertFalse($note->getIsLocal());
        static::assertSame(VisibilityScope::EVERYWHERE, $note->getScope());
        static::assertSame($object_uri, $note->getUrl());
        static::assertSame('en', $note->getLanguageLocale());
        static::assertSame('note', $note->getType());
        static::assertNull($note->getTitle());

        $ap_object = ActivitypubObject::getByPK(['object_uri' => $object_uri]);
        static::assertSame(Note::schemaName(), $ap_object->getObjectType());
        static::assertSame($object_uri, $ap_object->getObjectUri());
        static::assertSame($note->getId(), $ap_object->getObjectId());

        static::assertSame([], $note->getAttentionTargets());
        static::assertSame([], $note->getMentionTargets());
    }

    public function testNoteWithMentionFromJson()
    {
        self::bootKernel();

        $actor_uri         = 'https://instance.gnusocial.test/actor/42';
        $another_actor_uri = 'https://another-instance.gnusocial.test/actor/43';
        $object_uri        = 'https://instance.gnusocial.test/object/note/1340';
        $note              = ActivityPub::getObjectByUri($object_uri, try_online: false);
        static::assertInstanceOf(Note::class, $note);

        static::assertSame(Explorer::getOneFromUri($actor_uri, try_online: false)->getId(), $note->getActorId());
        static::assertSame('text/plain', $note->getContentType());
        static::assertSame('This is a public root note with a mention to @alice@another_instance.gnusocial.test.', $note->getContent());
        // TODO: implement proper sanitization rules
        //static::assertSame('<p>This is a public root note with a mention to @<span class=\"h-card\"><a href=\"https://another-instance.gnusocial.test/actor/43\" class=\"h-card u-url p-nickname mention\">alice@another_instance.gnusocial.test</a></span>.</p>', $note->getRendered());
        static::assertSame('ActivityPub', $note->getSource());
        static::assertNull($note->getReplyTo());
        static::assertFalse($note->getIsLocal());
        static::assertSame(VisibilityScope::EVERYWHERE, $note->getScope());
        static::assertSame($object_uri, $note->getUrl());
        static::assertSame('en', $note->getLanguageLocale());
        static::assertSame('note', $note->getType());
        static::assertNull($note->getTitle());

        $ap_object = ActivitypubObject::getByPK(['object_uri' => $object_uri]);
        static::assertSame(Note::schemaName(), $ap_object->getObjectType());
        static::assertSame($object_uri, $ap_object->getObjectUri());
        static::assertSame($note->getId(), $ap_object->getObjectId());

        static::assertCount(1, $attT = $note->getAttentionTargets());
        static::assertObjectEquals(Explorer::getOneFromUri($another_actor_uri, try_online: false), $attT[0]);
        static::assertSame([], $note->getMentionTargets());
    }
}
