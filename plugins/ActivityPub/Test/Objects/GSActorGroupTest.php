<?php

declare(strict_types = 1);

// {{{ License

// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.

// }}}

namespace Plugin\ActivityPub\Test\Objects;

use App\Entity\Actor;
use App\Util\GNUsocialTestCase;
use Plugin\ActivityPub\Entity\ActivitypubActor;
use Plugin\ActivityPub\Entity\ActivitypubRsa;
use Plugin\ActivityPub\Util\Explorer;

class GSActorGroupTest extends GNUsocialTestCase
{
    public function testGroupFromJson()
    {
        self::bootKernel();

        $group    = Explorer::getOneFromUri('https://instance.gnusocial.test/actor/21', try_online: false);
        $ap_group = ActivitypubActor::getByPK(['actor_id' => $group->getId()]);
        static::assertSame('https://instance.gnusocial.test/actor/21/inbox.json', $ap_group->getInboxUri());
        static::assertSame('https://instance.gnusocial.test/inbox.json', $ap_group->getInboxSharedUri());
        $group = $ap_group->getActor();
        static::assertSame('https://instance.gnusocial.test/actor/21', $group->getUri());
        static::assertSame(Actor::GROUP, $group->getType());
        static::assertSame('hackers', $group->getNickname());
        static::assertSame('Hackers!', $group->getFullname());
        $public_key = ActivityPubRsa::getByActor($group)->getPublicKey();
        static::assertSame("-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoZyKL+GyJbTV/ilVBlzz\n8OL/UwNi3KpfV5kQwXU0pPcBbw6y2JOfWnKUT1CfiHG3ntiOFnc+wQfHZk4hRSE8\n9Xe/G5Y215xW+gqx/kjt2GOENqzSzYXdEZ5Qsx6yumZD/yb6VZK9Og0HjX2mpRs9\nbactY76w4BQVntjZ17gSkMhYcyPFZTAIe7QDkeSPk5lkXfTwtaB3YcJSbQ3+s7La\npeEgukQDkrLUIP6cxayKrgUl4fhHdpx1Yk4Bzd/1XkZCjeBca94lP1p2M12amI+Z\nOLSTuLyEiCcku8aN+Ms9plwATmIDaGvKFVk0YVtBHdIJlYXV0yIscab3bqyhsLBK\njwIDAQAB\n-----END PUBLIC KEY-----\n", $public_key);
    }
}
