<?php

declare(strict_types = 1);

namespace Plugin\ActivityPub\Test\Fixtures;

use App\Core\DB;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Plugin\ActivityPub\Util\Model\Activity;
use Plugin\ActivityPub\Util\Model\Actor;
use Plugin\ActivityPub\Util\Model\Note;

class ActivityPubFixtures extends Fixture
{
    private static string $fixtures_path = __DIR__ . \DIRECTORY_SEPARATOR;

    public static function fixturesPath(string $path, string $ontology = 'gnusocial'): string
    {
        return self::$fixtures_path . $ontology . \DIRECTORY_SEPARATOR . $path;
    }

    public function load(ObjectManager $manager)
    {
        /*
         * Beware that it's important to Load Actors, Objects, Activities in this sequence
         * because we're running offline tests here.
         */
        $ontology = 'gnusocial';

        // Load Actors
        $person_path = self::fixturesPath('objects/person.jsonld', $ontology);
        $person      = Actor::fromJson(fread(fopen($person_path, 'r'), filesize($person_path)));
        DB::flush();
        $another_person_path = self::fixturesPath('objects/another_person.jsonld', $ontology);
        $another_person      = Actor::fromJson(fread(fopen($another_person_path, 'r'), filesize($another_person_path)));
        DB::flush();
        $group_path = self::fixturesPath('objects/group.jsonld', $ontology);
        $group      = Actor::fromJson(fread(fopen($group_path, 'r'), filesize($group_path)));
        DB::flush();

        // Load Objects
        $note_path = self::fixturesPath('objects/note.jsonld', $ontology);
        $note      = Note::fromJson(fread(fopen($note_path, 'r'), filesize($note_path)));
        DB::flush();
        $article_path = self::fixturesPath('objects/article.jsonld', $ontology);
        $article      = Note::fromJson(fread(fopen($article_path, 'r'), filesize($article_path)));
        DB::flush();
        $reply_path = self::fixturesPath('objects/reply.jsonld', $ontology);
        $reply      = Note::fromJson(fread(fopen($reply_path, 'r'), filesize($reply_path)));
        DB::flush();
        $note_with_mention_path = self::fixturesPath('objects/note_with_mention.jsonld', $ontology);
        $note_with_mention      = Note::fromJson(fread(fopen($note_with_mention_path, 'r'), filesize($note_with_mention_path)));
        DB::flush();

        // Load Activities
        $create_note_path = self::fixturesPath('activities/create_note.jsonld', $ontology);
        $create_note      = Activity::fromJson(fread(fopen($create_note_path, 'r'), filesize($create_note_path)));
        DB::flush();
        $create_article_path = self::fixturesPath('activities/create_article.jsonld', $ontology);
        $create_article      = Activity::fromJson(fread(fopen($create_article_path, 'r'), filesize($create_article_path)));
        DB::flush();
        $create_reply_path = self::fixturesPath('activities/create_reply.jsonld', $ontology);
        $create_reply      = Activity::fromJson(fread(fopen($create_reply_path, 'r'), filesize($create_reply_path)));
        DB::flush();
        $like_note_path = self::fixturesPath('activities/like_note.jsonld', $ontology);
        $like_note      = Activity::fromJson(fread(fopen($like_note_path, 'r'), filesize($like_note_path)));
        DB::flush();
    }
}
