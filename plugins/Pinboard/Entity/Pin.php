<?php

declare(strict_types = 1);

namespace Plugin\Pinboard\Entity;

use App\Core\Entity;
use App\Entity\Actor;
use App\Entity\LocalUser;
use App\Entity\Note;
use Component\Tag\Entity\NoteTag;
use DateTimeInterface;

class Pin extends Entity
{
    // {{{ Autocode
    // @codeCoverageIgnoreStart
    private int $note_id;
    private int $actor_id;
    private string $url_hash;
    private string $url;
    private string $title;
    private ?string $description = null;
    private bool $replace;
    private bool $public;
    private bool $unread;
    private DateTimeInterface $modified;

    public function setNoteId(int $note_id): self
    {
        $this->note_id = $note_id;
        return $this;
    }

    public function getNoteId(): int
    {
        return $this->note_id;
    }

    public function setActorId(int $actor_id): self
    {
        $this->actor_id = $actor_id;
        return $this;
    }

    public function getActorId(): int
    {
        return $this->actor_id;
    }

    public function setUrlHash(string $url_hash): self
    {
        $this->url_hash = mb_substr($url_hash, 0, 64);
        return $this;
    }

    public function getUrlHash(): string
    {
        return $this->url_hash;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;
        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setReplace(bool $replace): self
    {
        $this->replace = $replace;
        return $this;
    }

    public function getReplace(): bool
    {
        return $this->replace;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;
        return $this;
    }

    public function getPublic(): bool
    {
        return $this->public;
    }

    public function setUnread(bool $unread): self
    {
        $this->unread = $unread;
        return $this;
    }

    public function getUnread(): bool
    {
        return $this->unread;
    }

    public function setModified(DateTimeInterface $modified): self
    {
        $this->modified = $modified;
        return $this;
    }

    public function getModified(): DateTimeInterface
    {
        return $this->modified;
    }

    // @codeCoverageIgnoreEnd
    // }}} Autocode

    public const note_type = 'page';

    public static function cacheKeys(int|LocalUser|Actor $user): array
    {
        $id = \is_int($user) ? $user : $user->getId();
        return [
            'last-modified' => "pinboard-pin-{$id}-last-modified",
        ];
    }

    public function getNote(): Note
    {
        return Note::getById($this->getNoteId());
    }

    /**
     * @return NoteTag[]
     */
    public function getTags(): array
    {
        return Note::getById($this->getNoteId())->getTags();
    }

    public static function schemaDef(): array
    {
        return [
            'name'   => 'pinboard_pin',
            'fields' => [
                'note_id'     => ['type' => 'int',  'not null' => true, 'description' => 'Id of the note this pin created'],
                'actor_id'    => ['type' => 'int',  'not null' => true, 'description' => 'Actor who created this note'],
                'url_hash'    => ['type' => 'char', 'length' => 64, 'not null' => true, 'description' => 'Hash of the url, for indexing'],
                'url'         => ['type' => 'text', 'not null' => true, 'description' => 'Plain URL this pin refers to (gets rendered in the corresponding note, useful for replace)'],
                'title'       => ['type' => 'text', 'not null' => true, 'description' => 'Title given to this bookmark'],
                'description' => ['type' => 'text', 'description' => 'Description given to this bookmark'],
                'replace'     => ['type' => 'bool', 'not null' => true, 'description' => 'Replace any existing bookmark with this URL. Default is yes. If set to no, will throw an error if bookmark exists'],
                'public'      => ['type' => 'bool', 'not null' => true, 'description' => 'Whether private or public'],
                'unread'      => ['type' => 'bool', 'not null' => true, 'description' => 'Has this been read'],
                'modified'    => ['type' => 'timestamp', 'not null' => true, 'description' => 'date this record was modified'],
            ],
            'primary key' => ['actor_id', 'url_hash'],
            'indexes'     => [
                'actor_modified_idx' => ['actor_id', 'modified'],
            ],
        ];
    }
}
