<?php

declare(strict_types = 1);

// {{{ License
// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.
// }}}

/**
 * Pinboard server API, doesn't (currently) allow importing from the
 * official website
 *
 * @package   GNUsocial
 *
 * @author    Hugo Sales <hugo@hsal.es>
 * @copyright 2022 Free Software Foundation, Inc http://www.fsf.org
 * @license   https://www.gnu.org/licenses/agpl.html GNU AGPL v3 or later
 */

namespace Plugin\Pinboard;

use App\Core\Event;
use App\Core\Modules\Plugin;
use App\Core\Router;
use EventResult;
use Plugin\Pinboard\Controller as C;
use Symfony\Component\HttpFoundation\Request;

class Pinboard extends Plugin
{
    public const controller_route = 'pinboard_settings';

    public function onAddRoute(Router $r): EventResult
    {
        $r->connect(self::controller_route, '/pinboard/settings', C\Settings::class, options: ['method' => 'post']);

        $r->connect('pinboard_posts_update', '/pinboard/v1/posts/update', [C\APIv1::class, 'posts_update']);
        $r->connect('pinboard_posts_add', '/pinboard/v1/posts/add', [C\APIv1::class, 'posts_add']);
        $r->connect('pinboard_posts_delete', '/pinboard/v1/posts/delete', [C\APIv1::class, 'posts_delete']);
        $r->connect('pinboard_posts_get', '/pinboard/v1/posts/get', [C\APIv1::class, 'posts_get']);
        $r->connect('pinboard_posts_recent', '/pinboard/v1/posts/recent', [C\APIv1::class, 'posts_recent']);
        $r->connect('pinboard_posts_all', '/pinboard/v1/posts/all', [C\APIv1::class, 'posts_all']);

        $r->connect('pinboard_posts_dates', '/pinboard/v1/posts/dates', [C\APIv1::class, 'unimplemented']);
        $r->connect('pinboard_posts_suggest', '/pinboard/v1/posts/suggest', [C\APIv1::class, 'unimplemented']);

        $r->connect('pinboard_tags_get', '/pinboard/v1/tags/get', [C\APIv1::class, 'tags_get']);

        $r->connect('pinboard_tags_delete', '/pinboard/v1/tags/delete', [C\APIv1::class, 'unimplemented']);
        $r->connect('pinboard_tags_rename', '/pinboard/v1/tags/rename', [C\APIv1::class, 'unimplemented']);
        $r->connect('pinboard_user_secret', '/pinboard/v1/user/secret', [C\APIv1::class, 'unimplemented']);
        $r->connect('pinboard_user_api_token', '/pinboard/v1/user/api_token', [C\APIv1::class, 'unimplemented']);
        $r->connect('pinboard_notes_list', '/pinboard/v1/notes/list', [C\APIv1::class, 'unimplemented']);
        $r->connect('pinboard_notes_id', '/pinboard/v1/notes/{id}', [C\APIv1::class, 'unimplemented']);

        return Event::next;
    }

    /**
     * @param SettingsTabsType $tabs
     */
    public function onPopulateSettingsTabs(Request $request, string $section, array &$tabs): EventResult
    {
        if ($section === 'api') {
            $tabs[] = [
                'title'      => 'Pinboard',
                'desc'       => 'Pinboard API settings',
                'id'         => 'settings-pinboard-api',
                'controller' => C\Settings::setup(),
            ];
        }
        return Event::next;
    }
}
