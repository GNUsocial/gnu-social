<?php

declare(strict_types = 1);

namespace Plugin\UnboundGroup\Entity;

use App\Core\Entity;

class activitypubGroupUnbound extends Entity
{
    // These tags are meant to be literally included and will be populated with the appropriate fields, setters and getters by `bin/generate_entity_fields`
    // {{{ Autocode
    // @codeCoverageIgnoreStart
    private int $actor_id;
    private ?bool $unbound = null;

    public function setActorId(int $actor_id): self
    {
        $this->actor_id = $actor_id;
        return $this;
    }

    public function getActorId(): int
    {
        return $this->actor_id;
    }

    public function setUnbound(?bool $unbound): self
    {
        $this->unbound = $unbound;
        return $this;
    }

    public function getUnbound(): ?bool
    {
        return $this->unbound;
    }

    // @codeCoverageIgnoreEnd
    // }}} Autocode

    public static function schemaDef(): array
    {
        return [
            'name'   => 'activitypubGroupUnbound',
            'fields' => [
                'actor_id' => ['type' => 'int', 'foreign key' => true, 'target' => 'Actor.id', 'multiplicity' => 'one to one', 'not null' => true, 'description' => 'foreign key to actor table'],
                'unbound'  => ['type' => 'bool', 'not null' => false, 'description' => 'gs:unbound'],
            ],
            'primary key' => ['actor_id'],
        ];
    }
}
