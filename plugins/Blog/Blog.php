<?php

declare(strict_types = 1);
// {{{ License

// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.

// }}}

namespace Plugin\Blog;

use App\Core\Event;
use function App\Core\I18n\_m;
use App\Core\Modules\Plugin;
use App\Core\Router;
use App\Util\Common;
use App\Util\HTML;
use EventResult;
use Plugin\Blog\Controller as C;

class Blog extends Plugin
{
    public function onAddRoute(Router $r): EventResult
    {
        $r->connect(id: 'blog_post', uri_path: '/blog/post', target: [C\Post::class, 'makePost']);
        return Event::next;
    }

    /**
     * @param (array{actor: \App\Entity\Actor} & array<string, mixed>) $vars
     * @param array<string> $res
     */
    public function onAppendCardProfile(array $vars, array &$res): EventResult
    {
        $actor = Common::actor();
        $group = $vars['actor'];
        if (!\is_null($actor) && $group->isGroup()) {
            $res[] = HTML::html(['a' => ['attrs' => ['href' => Router::url('blog_post', ['in' => $group->getId()]), 'title' => _m('Make a new blog post'), 'class' => 'profile-extra-actions'], _m('Post in blog')]]);
        }
        return Event::next;
    }
}
