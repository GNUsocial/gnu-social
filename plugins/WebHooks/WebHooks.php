<?php

declare(strict_types = 1);

// {{{ License
// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.
// }}}

namespace Plugin\WebHooks;

use App\Core\DB;
use App\Core\Event;
use App\Core\HTTPClient;
use App\Core\Log;
use App\Core\Modules\Plugin;
use App\Core\Queue;
use App\Core\Router;
use App\Entity\Activity;
use App\Entity\Actor;
use App\Entity\LocalUser;
use App\Util\Common;
use App\Util\Exception\ServerException;
use EventResult;
use Exception;
use Functional as F;
use Plugin\WebHooks\Controller as C;
use Plugin\WebHooks\Entity as E;
use Symfony\Component\HttpFoundation\Request;

/**
 * @phpstan-type AliasNotifications array{ sender: Actor, activity: Activity, effective_targets: array<Actor>, reason: ?string }
 * @phpstan-type AliasSubscriptions array{ subscriber: Actor, activity: Activity, target: Actor, reason: ?string }
 */
class WebHooks extends Plugin
{
    public const controller_route = 'webhook';

    public function onAddRoute(Router $r): EventResult
    {
        $r->connect(self::controller_route, '/webhook-settings', C\WebHooks::class, options: ['method' => 'post']);
        return EventResult::next;
    }

    /**
     * @param SettingsTabsType $tabs
     */
    public function onPopulateSettingsTabs(Request $request, string $section, array &$tabs): EventResult
    {
        if ($section === 'api') {
            $tabs[] = [
                'title'      => 'Web Hooks',
                'desc'       => 'Add hooks that run when an internal event occurs, allowing your third party resource to react',
                'id'         => 'settings-webhooks',
                'controller' => C\WebHooks::setup(),
            ];
        }
        return Event::next;
    }

    private function maybeEnqueue(Actor $actor, string $event, array $args): void
    {
        $hook_target = DB::findOneBy(E\WebHook::class, ['actor_id' => $actor->getId(), 'event' => $event], return_null: true)?->getTarget();
        if (!\is_null($hook_target)) {
            Queue::enqueue([$event, $hook_target, $actor, $args], queue: 'webhook');
        }
    }

    public function onNewNotificationEnd(Actor $sender, Activity $activity, array $effective_targets, ?string $reason): EventResult
    {
        foreach ($effective_targets as $actor) {
            $this->maybeEnqueue($actor, 'notifications', [$sender, $activity, $effective_targets, $reason]);
        }
        return Event::next;
    }

    public function onNewSubscriptionEnd(LocalUser|Actor $subscriber, Activity $activity, Actor $hook_target, ?string $reason): EventResult
    {
        $this->maybeEnqueue($hook_target, 'subscriptions', [$subscriber, $activity, $hook_target, $reason]);
        return Event::next;
    }

    /**
     * @param AliasNotifications|AliasSubscriptions $args
     */
    public function onQueueWebhook(string $type, string $hook_target, Actor $actor, array $args): EventResult
    {
        switch ($type) {
            case 'notifications':
                ['sender' => $sender, 'activity' => $activity, 'effective_targets' => $targets, 'reason' => $reason] = $args;
                $data                                                                                                = [
                    'type'     => 'notification',
                    'activity' => '%activity%',
                    'actor'    => ['id' => $sender->getId(), 'nickname' => $sender->getNickname()],
                    'targets'  => F\map(array_values($targets), fn (Actor $actor) => ['id' => $actor->getId(), 'nickname' => $actor->getNickname()]),
                    'reason'   => $reason,
                ];
                break;
            case 'subscriptions':
                ['subscriber' => $subscriber, 'activity' => $activity, 'target' => $target, 'reason' => $reason] = $args;
                $data                                                                                            = [
                    'type'     => 'subscription',
                    'activity' => '%activity%',
                    'actor'    => ['id' => $subscriber->getId(), 'nickname' => $subscriber->getNickname()],
                    'targets'  => [['id' => $target->getId(), 'nickname' => $target->getNickname()]],
                    'reason'   => $reason,
                ];
                break;
            default:
                throw new ServerException("Webhook notification handler for event {$type} not implemented");
        }

        // toJson(Activity) is already JSON (hopefully that's obvious :') ), so replace it after converting the rest to JSON
        $json = str_replace('"activity":"%activity%"', '"activity":' . \Plugin\ActivityPub\Util\Model\Activity::toJson($activity), json_encode($data));
        Log::debug("WebHooks: POST {$hook_target} on behalf of actor {$actor->getId()} ({$actor->getNickname()})", [$data, ['json' => $json]]);
        try {
            $method = Common::config('plugin_webhooks', 'method');
            HTTPClient::{$method}($hook_target, ['body' => $json, 'headers' => ['content-type' => 'application/json', 'user-agent' => 'GNU social']]);
        } catch (Exception $e) {
            Log::debug("WebHooks: Failed POST {$hook_target} on behalf of actor {$actor->getId()} ({$actor->getNickname()})", [$e]);
        }
        return Event::stop;
    }
}
