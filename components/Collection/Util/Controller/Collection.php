<?php

declare(strict_types = 1);

namespace Component\Collection\Util\Controller;

use App\Core\Controller;
use App\Entity\Actor;
use App\Entity\Note;
use App\Util\Common;
use Component\Collection\Collection as CollectionComponent;

/**
 * @template T
 */
abstract class Collection extends Controller
{
    /**
     * @param array<string, OrderByType> $note_order_by
     * @param array<string, OrderByType> $actor_order_by
     *
     * @return array{notes: null|Note[], actors: null|Actor[]}
     */
    public function query(string $query, ?string $locale = null, ?Actor $actor = null, array $note_order_by = [], array $actor_order_by = []): array
    {
        $actor  ??= Common::actor();
        $locale ??= Common::currentLanguage()->getLocale();
        return CollectionComponent::query($query, $this->int('page') ?? 1, $locale, $actor, $note_order_by, $actor_order_by);
    }
}
