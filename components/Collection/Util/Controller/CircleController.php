<?php

declare(strict_types = 1);

namespace Component\Collection\Util\Controller;

/**
 * @extends OrderedCollection<\Component\Circle\Entity\ActorCircle>
 */
class CircleController extends OrderedCollection
{
}
