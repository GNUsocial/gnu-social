<?php

declare(strict_types = 1);

namespace Component\Collection\Util\Controller;

/**
 * @template T
 *
 * @extends Collection<T>
 */
abstract class OrderedCollection extends Collection
{
}
