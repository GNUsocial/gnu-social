<?php

declare(strict_types = 1);

// {{{ License
// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.
// }}}

namespace Component\Notification\Entity;

use App\Core\Entity;

/**
 * Entity for object attentions
 *
 * An attention is a form of persistent notification.
 * It exists together and for as long as the object it belongs to.
 * Creating an attention requires creating a Notification.
 *
 * @category  DB
 * @package   GNUsocial
 *
 * @author    Zach Copley <zach@status.net>
 * @copyright 2010 StatusNet Inc.
 * @author    Mikael Nordfeldth <mmn@hethane.se>
 * @copyright 2009-2014 Free Software Foundation, Inc http://www.fsf.org
 * @author    Diogo Peralta Cordeiro <@diogo.site>
 * @copyright 2021-2022 Free Software Foundation, Inc http://www.fsf.org
 * @license   https://www.gnu.org/licenses/agpl.html GNU AGPL v3 or later
 */
class Attention extends Entity
{
    // {{{ Autocode
    // @codeCoverageIgnoreStart
    private string $object_type;
    private int $object_id;
    private int $target_id;

    public function setObjectType(string $object_type): self
    {
        $this->object_type = mb_substr($object_type, 0, 32);
        return $this;
    }

    public function getObjectType(): string
    {
        return $this->object_type;
    }

    public function setObjectId(int $object_id): self
    {
        $this->object_id = $object_id;
        return $this;
    }

    public function getObjectId(): int
    {
        return $this->object_id;
    }

    public function setTargetId(int $target_id): self
    {
        $this->target_id = $target_id;
        return $this;
    }

    public function getTargetId(): int
    {
        return $this->target_id;
    }

    // @codeCoverageIgnoreEnd
    // }}} Autocode

    public static function schemaDef(): array
    {
        return [
            'name'        => 'attention',
            'description' => 'Attentions to actors (these are not mentions)',
            'fields'      => [
                'object_type' => ['type' => 'varchar',  'length' => 32,     'not null' => true, 'description' => 'the name of the table this object refers to'],
                'object_id'   => ['type' => 'int',      'not null' => true, 'description' => 'id in the referenced table'],
                'target_id'   => ['type' => 'int', 'foreign key' => true, 'target' => 'Actor.id', 'multiplicity' => 'one to one', 'not null' => true, 'description' => 'actor_id for feed receiver'],
            ],
            'primary key' => ['object_type', 'object_id', 'target_id'],
            'indexes'     => [
                'attention_object_id_idx' => ['object_id'],
                'attention_target_id_idx' => ['target_id'],
            ],
        ];
    }
}
