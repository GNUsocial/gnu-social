<?php

declare(strict_types = 1);

// {{{ License
// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.
// }}}

namespace Component\Group;

use App\Core\Event;
use function App\Core\I18n\_m;
use App\Core\Modules\Component;
use App\Core\Router;
use App\Entity\Activity;
use App\Entity\Actor;
use App\Util\Common;
use App\Util\HTML;
use App\Util\Nickname;
use Component\Group\Controller as C;
use Component\Group\Entity\LocalGroup;
use Component\Notification\Notification;
use EventResult;
use Symfony\Component\HttpFoundation\Request;

class Group extends Component
{
    public function onAddRoute(Router $r): EventResult
    {
        $r->connect(id: 'group_actor_view_id', uri_path: '/group/{id<\d+>}', target: [C\GroupFeed::class, 'groupViewId']);
        $r->connect(id: 'group_actor_view_nickname', uri_path: '/!{nickname<' . Nickname::DISPLAY_FMT . '>}', target: [C\GroupFeed::class, 'groupViewNickname']);
        $r->connect(id: 'group_create', uri_path: '/group/new', target: [C\Group::class, 'groupCreate']);
        $r->connect(id: 'group_actor_settings', uri_path: '/group/{id<\d+>}/settings', target: [C\Group::class, 'groupSettings']);
        return Event::next;
    }

    /**
     * Enqueues a notification for an Actor (such as person or group) which means
     * it shows up in their home feed and such.
     *
     * @param Actor[] $targets
     */
    public function onNewNotificationStart(Actor $sender, Activity $activity, array $targets = [], ?string $reason = null): EventResult
    {
        foreach ($targets as $target) {
            if ($target->isGroup()) {
                // The Group announces to its subscribers
                Notification::notify(
                    sender: $target,
                    activity: $activity,
                    targets: $target->getSubscribers(),
                    reason: $reason,
                );
            }
        }

        return Event::next;
    }

    /**
     * Add an <a href=group_actor_settings> to the profile card for groups, if the current actor can access them
     *
     * @param array<string, mixed> $vars
     * @param string[]             $res
     */
    public function onAppendCardProfile(array $vars, array &$res): EventResult
    {
        $actor = Common::actor();
        $group = $vars['actor'];
        if (!\is_null($actor) && $group->isGroup()) {
            if ($actor->canModerate($group)) {
                $url   = Router::url('group_actor_settings', ['id' => $group->getId()]);
                $res[] = HTML::html(['a' => ['attrs' => ['href' => $url, 'title' => _m('Edit group settings'), 'class' => 'profile-extra-actions'], _m('Group settings')]]);
            }
        }
        return Event::next;
    }

    /**
     * If in a group route, get the current group
     */
    private function getGroupFromContext(Request $request): ?Actor
    {
        if (\is_array($request->get('post_note')) && \array_key_exists('_next', $request->get('post_note'))) {
            $next       = parse_url($request->get('post_note')['_next']);
            $match      = Router::match($next['path']);
            $route      = $match['_route'];
            $identifier = $match['id'] ?? $match['nickname'] ?? null;
        } else {
            $route      = $request->get('_route');
            $identifier = $request->get('id') ?? $request->get('nickname');
        }
        if (str_starts_with($route, 'group_actor_view_')) {
            switch ($route) {
                case 'group_actor_view_nickname':
                    return LocalGroup::getActorByNickname($identifier);
                case 'group_actor_view_id':
                    return Actor::getById((int) $identifier);
            }
        }
        return null;
    }

    /**
     * @param Actor[] $targets
     */
    public function onPostingFillTargetChoices(Request $request, Actor $actor, array &$targets): EventResult
    {
        $group = $this->getGroupFromContext($request);
        if (!\is_null($group)) {
            $nick           = "!{$group->getNickname()}";
            $targets[$nick] = $group->getId();
        }
        return Event::next;
    }

    /**
     * Indicates the context in which Posting's form is to be presented. Passing on $context_actor to Posting's
     * onAppendRightPostingBlock event, the Group a given $actor is currently browsing.
     *
     * Makes it possible to automagically fill in the targets (aka the Group which this $request route is connected to)
     * in the Posting's form.
     *
     * @param null|Actor $context_actor Actor group, if current route is part of an existing Group set of routes
     */
    public function onPostingGetContextActor(Request $request, Actor $actor, ?Actor &$context_actor): EventResult
    {
        $ctx = $this->getGroupFromContext($request);
        if (!\is_null($ctx)) {
            $context_actor = $ctx;
            return Event::stop;
        }
        return Event::next;
    }
}
