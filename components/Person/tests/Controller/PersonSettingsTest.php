<?php

declare(strict_types = 1);

// {{{ License

// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.

// }}}

namespace Component\Person\tests\Controller;

use App\Core\DB;
use App\Core\Router;
use App\Entity\LocalUser;
use App\Util\GNUsocialTestCase;
use Jchook\AssertThrows\AssertThrows;

class PersonSettingsTest extends GNUsocialTestCase
{
    use AssertThrows;

    public function testPersonalInfo()
    {
        $client = static::createClient();
        $user   = DB::findOneBy(LocalUser::class, ['nickname' => 'form_personal_info_test_user']);
        $client->loginUser($user);

        $client->request('GET', Router::url('person_actor_settings', ['id' => $user->getId()]));
        $this->assertResponseIsSuccessful();
        $crawler = $client->submitForm('Save personal info', [
            'save_personal_info[nickname]'  => 'form_test_user_new_nickname',
            'save_personal_info[full_name]' => 'Form User',
            'save_personal_info[homepage]'  => 'https://gnu.org',
            'save_personal_info[bio]'       => 'I was born at a very young age',
            'save_personal_info[location]'  => 'right here',
            //            'save_personal_info[phone_number]' => '+351908555842', // from fakenumber.net
        ]);
        $changed_user = DB::findOneBy(LocalUser::class, ['id' => $user->getId()]);
        $actor        = $changed_user->getActor();
        static::assertSame('form_test_user_new_nickname', $changed_user->getNickname());
        static::assertSame('form_test_user_new_nickname', $actor->getNickname());
        static::assertSame('Form User', $actor->getFullName());
        static::assertSame('https://gnu.org', $actor->getHomepage());
        static::assertSame('I was born at a very young age', $actor->getBio());
        static::assertSame('right here', $actor->getLocation());
//        static::assertSame('908555842', $changed_user->getPhoneNumber()->getNationalNumber());
    }

    public function testEmail()
    {
        $client = static::createClient();
        $user   = DB::findOneBy(LocalUser::class, ['nickname' => 'form_account_test_user']);
        $client->loginUser($user);

        $client->request('GET', Router::url('person_actor_settings', ['id' => $user->getId()]));
        $this->assertResponseIsSuccessful();
        $crawler = $client->submitForm('Save email info', [
            'save_email[outgoing_email_sanitized]' => 'outgoing@provider.any',
            'save_email[incoming_email_sanitized]' => 'incoming@provider.any',
        ]);

        $changed_user = DB::findOneBy(LocalUser::class, ['id' => $user->getId()]);
        static::assertSame($changed_user->getOutgoingEmail(), 'outgoing@provider.any');
        static::assertSame($changed_user->getIncomingEmail(), 'incoming@provider.any');
    }

    public function testCorrectPassword()
    {
        $client = static::createClient();
        $user   = DB::findOneBy(LocalUser::class, ['nickname' => 'form_account_test_user']);
        $client->loginUser($user);

        $client->request('GET', Router::url('person_actor_settings', ['id' => $user->getId()]));
        $this->assertResponseIsSuccessful();
        $crawler = $client->submitForm('Save new password', [
            'save_password[old_password]'     => 'foobar',
            'save_password[password][first]'  => 'this is some test password',
            'save_password[password][second]' => 'this is some test password',
        ]);

        $changed_user = DB::findOneBy(LocalUser::class, ['id' => $user->getId()]);
        static::assertTrue($changed_user->checkPassword('this is some test password'));
    }

    public function testAccountWrongPassword()
    {
        $client = static::createClient();
        $user   = DB::findOneBy(LocalUser::class, ['nickname' => 'form_account_test_user']);
        $client->loginUser($user);

        $client->request('GET', Router::url('person_actor_settings', ['id' => $user->getId()]));
        $this->assertResponseIsSuccessful();
        $crawler = $client->submitForm('Save new password', [
            'save_password[old_password]'     => 'some wrong password',
            'save_password[password][first]'  => 'this is some test password',
            'save_password[password][second]' => 'this is some test password',
        ]);
        $this->assertResponseStatusCodeSame(500); // 401 in future
        $this->assertSelectorTextContains('.stacktrace', 'AuthenticationException');
    }

// TODO: First actually implement this functionality
//    public function testNotifications()
//    {
//        $client = static::createClient();
//        $user   = DB::findOneBy(LocalUser::class, ['nickname' => 'form_account_test_user']);
//        $client->loginUser($user);
//
//        $client->request('GET', Router::url('person_actor_settings', ['id' => $user->getId()]));
//        $this->assertResponseIsSuccessful();
//        $crawler = $client->submitForm('Save notification settings for Email', [
//            'save_email[activity_by_subscribed]' => false,
//            'save_email[mention]'                => true,
//            'save_email[reply]'                  => false,
//            'save_email[subscription]'           => true,
//            'save_email[favorite]'               => false,
//            'save_email[nudge]'                  => true,
//            'save_email[dm]'                     => false,
//            'save_email[enable_posting]'         => true,
//        ]);
//        $settings = DB::findOneBy('user_notification_prefs', ['user_id' => $user->getId(), 'transport' => 'email']);
//        static::assertSame($settings->getActivityBySubscribed(), false);
//        static::assertSame($settings->getMention(), true);
//        static::assertSame($settings->getReply(), false);
//        static::assertSame($settings->getSubscription(), true);
//        static::assertSame($settings->getFavorite(), false);
//        static::assertSame($settings->getNudge(), true);
//        static::assertSame($settings->getDm(), false);
//        static::assertSame($settings->getEnablePosting(), true);
//    }
}
