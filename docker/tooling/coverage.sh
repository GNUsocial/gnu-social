#!/bin/sh

cd /var/www/social || exit 1

php -dpcov.enabled=1 -dpcov.directory=. -dpcov.exclude="~vendor~" vendor/bin/phpunit --configuration=phpunit.xml.dist $*
# XDEBUG_MODE=off vendor/bin/paratest --processes=32 --functional --configuration=phpunit.xml.dist $*
