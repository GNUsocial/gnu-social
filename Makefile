
DIR=$(strip $(notdir $(CURDIR))) # Seems a bit hack-ish, but `basename` works differently

translate-container-name = $$(if docker container inspect $(1) > /dev/null 2>&1; then echo $(1); else echo $(1) | sed 'y/_/-/' ; fi)
args = `arg="$(filter-out $@,$(MAKECMDGOALS))" && echo $${arg:-${1}}`

%:
	@:

.PHONY:
	@if ! docker info > /dev/null; then echo "Docker does not seem to be running"; exit 1; fi

up: .PHONY
	docker-compose up -d

down: .PHONY
	docker-compose down

redis-shell:
	docker exec -it $(call translate-container-name,$(strip $(DIR))_redis_1) sh -c 'redis-cli'

php-repl: .PHONY
	docker exec -it $(call translate-container-name,$(strip $(DIR))_php_1) sh -c '/var/www/social/bin/console psysh'

php-shell: .PHONY
	docker exec -it $(call translate-container-name,$(strip $(DIR))_php_1) sh -c 'cd /var/www/social; sh'

psql-shell: .PHONY
	docker exec -it $(call translate-container-name,$(strip $(DIR))_db_1) sh -c "psql -U postgres social"

database-force-nuke:
	docker stop $(call translate-container-name,$(strip $(DIR))_worker_1) \
	&& docker exec -it $(call translate-container-name,$(strip $(DIR))_php_1) sh -c "cd /var/www/social; bin/console doctrine:database:drop --force && bin/console doctrine:database:create && bin/console doctrine:schema:update --dump-sql --force && bin/console app:populate_initial_values" \
	&& docker-compose up -d

database-force-schema-update:
	docker exec -it $(call translate-container-name,$(strip $(DIR))_php_1) sh -c "/var/www/social/bin/console doctrine:schema:update --dump-sql --force"

tooling-docker-up: .PHONY
	@sh -c 'if [ ! docker container inspect $(call translate-container-name,tooling_php_1) > /dev/null 2>&1 ]; then cd docker/tooling && docker-compose up -d --build > /dev/null 2>&1; fi'

tooling-docker-down: .PHONY
	cd docker/tooling && docker-compose down

test: tooling-docker-up
	@docker exec $(call translate-container-name,tooling_php_1) /var/tooling/coverage.sh $(call args,'')

test-database-force-nuke: tooling-docker-up
	docker exec -it $(call translate-container-name,tooling_php_1) sh -c 'cd /var/www/social; bin/console doctrine:database:drop --force'

tooling-php-shell: tooling-docker-up
	docker exec -it $(call translate-container-name,tooling_php_1) sh

test-accesibility: tooling-docker-up
	cd docker/tooling && docker-compose run pa11y /accessibility.sh

cs-fixer: tooling-docker-up
	@bin/php-cs-fixer $${CS_FIXER_FILE}

doc-check: tooling-docker-up
	bin/php-doc-check

phpstan: tooling-docker-up
	bin/phpstan

remove-var:
	rm -rf var/*

remove-file:
	sudo rm -rf file/*

flush-redis-cache:
	docker exec -it $(call translate-container-name,$(strip $(DIR))_redis_1) sh -c 'redis-cli flushall'

install-plugins:
	docker exec -it $(call translate-container-name,$(strip $(DIR))_php_1) /var/www/social/bin/install_plugins.sh

update-dependencies:
	docker exec -it $(call translate-container-name,$(strip $(DIR))_php_1) sh -c 'cd /var/www/social && composer update'

update-autocode:
	docker exec -it $(call translate-container-name,$(strip $(DIR))_php_1) sh -c 'cd /var/www/social && bin/update_autocode'

backup-actors:
	docker exec -it $(call translate-container-name,$(strip $(DIR))_db_1) \
	sh -c 'su postgres -c "mkdir -p /tmp/backup"' && \
	docker exec -it $(call translate-container-name,$(strip $(DIR))_php_1) \
	sh -c "cd /var/www/social && bin/console doctrine:query:sql \"\
	copy actor to '/tmp/backup/actor.csv';\
	copy local_user to '/tmp/backup/local_user.csv';\
	copy local_group to '/tmp/backup/local_group.csv';\
	\
	copy activitypub_actor to '/tmp/backup/ap_actor.csv';\
	copy activitypub_rsa to '/tmp/backup/ap_rsa.csv';\
	\
	copy actor_subscription to '/tmp/backup/actor_subscription.csv';\
	copy group_member to '/tmp/backup/group_member.csv';\
	\
	copy feed to '/tmp/backup/feed.csv';\
	copy (SELECT 'ALTER SEQUENCE ' || c.relname || ' RESTART WITH ' || nextval(c.relname::regclass) || ';'\
	FROM pg_class c WHERE c.relkind = 'S') to '/tmp/backup/sequences';\"" && \
	mkdir -p /tmp/social-sql-backup && \
	docker cp $(call translate-container-name,$(strip $(DIR))_db_1):/tmp/backup/. /tmp/social-sql-backup

restore-actors:
	docker cp /tmp/social-sql-backup/. $(call translate-container-name,$(strip $(DIR))_db_1):/tmp/backup
	docker exec -it $(call translate-container-name,$(strip $(DIR))_db_1) sh -c 'chown postgres /tmp/backup' && \
	docker exec -it $(call translate-container-name,$(strip $(DIR))_php_1) \
	sh -c "cd /var/www/social && bin/console doctrine:query:sql \"\
	copy actor from '/tmp/backup/actor.csv';\
	copy local_user from '/tmp/backup/local_user.csv';\
	copy local_group from '/tmp/backup/local_group.csv';\
	\
	copy activitypub_actor from '/tmp/backup/ap_actor.csv';\
	copy activitypub_rsa from '/tmp/backup/ap_rsa.csv';\
	\
	copy actor_subscription from '/tmp/backup/actor_subscription.csv';\
	copy group_member from '/tmp/backup/group_member.csv';\
	\
	copy feed from '/tmp/backup/feed.csv';\
	`cat /tmp/social-sql-backup/sequences`\""

force-nuke-everything: down remove-var remove-file up flush-redis-cache database-force-nuke install-plugins

force-delete-content: backup-actors force-nuke-everything restore-actors
