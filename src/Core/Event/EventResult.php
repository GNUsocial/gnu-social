<?php

declare(strict_types = 1);

/**
 * An event must return a value from this enum, which is in the global namespace as \EventResult
 */
enum EventResult
{
    case stop;
    case next;
    case unhandled;
}
