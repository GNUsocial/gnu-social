<?php

declare(strict_types = 1);

namespace App\Core\Event;

use EventResult;
use Symfony\Component\EventDispatcher\GenericEvent;

class GSEvent extends GenericEvent
{
    protected mixed $result;

    public function setResult(mixed $result): void
    {
        $this->result = $result;
    }

    public function getResult(): mixed
    {
        return $this->result ?? EventResult::next;
    }
}
