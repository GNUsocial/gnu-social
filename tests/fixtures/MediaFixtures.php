<?php

declare(strict_types = 1);

namespace App\Test\Fixtures;

use App\Core\ActorLocalRoles;
use App\Core\DB;
use App\Core\GSFile;
use App\Entity\Activity;
use App\Entity\Actor;
use App\Entity\Note;
use App\Util\TemporaryFile;
use Component\Attachment\Entity\ActorToAttachment;
use Component\Attachment\Entity\AttachmentToNote;
use Component\Conversation\Conversation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Functional as F;

class MediaFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        DB::setManager($manager);
        $actor = Actor::create(['nickname' => 'attachment_test_actor', 'is_local' => false, 'roles' => ActorLocalRoles::PARTICIPANT | ActorLocalRoles::VISITOR, 'type' => Actor::PERSON]);
        $manager->persist($actor);
        $note = Note::create(['actor_id' => $actor->getId(), 'content' => 'some other content', 'content_type' => 'text/plain', 'is_local' => true]);
        $manager->persist($note);
        $activity = Activity::create(['actor_id' => $actor->getId(), 'verb' => 'create', 'object_type' => 'note', 'object_id' => $note->getId(), 'source' => 'auto-test']);
        Conversation::assignLocalConversation($note, null);
        $manager->persist($activity);
        F\map(
            glob(INSTALLDIR . '/tests/sample-uploads/*'),
            function (string $filepath) use ($manager, $actor, $note) {
                $file = new TemporaryFile();
                $file->write(file_get_contents($filepath));
                try {
                    $a = GSFile::storeFileAsAttachment($file);
                    $manager->persist(ActorToAttachment::create(['attachment_id' => $a->getId(), 'actor_id' => $actor->getId()]));
                    $manager->persist(AttachmentToNote::create(['attachment_id' => $a->getId(), 'note_id' => $note->getId(), 'title' => mb_substr($filepath, mb_strrpos($filepath, '/') + 1)]));
                    $a->livesIncrementAndGet();
                } catch (Exception $e) {
                    echo "Could not save file {$filepath}, failed with {$e}\n";
                } finally {
                    unset($file);
                }
            },
        );
        $manager->flush();
    }
}
