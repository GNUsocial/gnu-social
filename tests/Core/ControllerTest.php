<?php

declare(strict_types = 1);

// {{{ License

// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.

// }}}

namespace App\Test\Core;

use App\Util\GNUsocialTestCase;
use Jchook\AssertThrows\AssertThrows;

class ControllerTest extends GNUsocialTestCase
{
    use AssertThrows;

    public function testJSONRequest()
    {
        // `server` will populate $_SERVER on the other side
        $client = static::createClient(options: [], server: ['HTTP_ACCEPT' => 'application/json']);
        $client->request('GET', '/feed/public');
        $this->assertResponseIsSuccessful();
        $response = $client->getResponse();
        static::assertTrue(mb_strpos($response->headers->get('content-type'), 'json') !== false);
        static::assertJson($response->getContent());
    }
}
